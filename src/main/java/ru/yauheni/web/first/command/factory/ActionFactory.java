package ru.yauheni.web.first.command.factory;

import ru.yauheni.web.first.command.ActionCommand;
import ru.yauheni.web.first.command.EmptyCommand;
import ru.yauheni.web.first.command.client.CommandEnum;

import javax.servlet.http.HttpServletRequest;

public class ActionFactory {

    public ActionCommand defineCommand(HttpServletRequest request) {
        ActionCommand command = new EmptyCommand();

        String action = request.getParameter("command");
        if (action == null || action.isEmpty()) {
            return command;
        }

        CommandEnum currentEnum = CommandEnum.valueOf(action.toUpperCase());
        command = currentEnum.getCurrentCommand();

        return command;
    }
}
