package ru.yauheni.web.first.command;

import ru.yauheni.web.first.logic.LoginLogic;
import ru.yauheni.web.first.manager.ConfigurationManager;
import ru.yauheni.web.first.manager.MessageManager;

import javax.servlet.http.HttpServletRequest;

public class LoginCommand implements ActionCommand {

    private static final String PARAM_NAME_LOGIN = "login";
    private static final String PARAM_NAME_PASSWORD = "password";

    public String execute(HttpServletRequest req) {
        String page;

        String login = req.getParameter(PARAM_NAME_LOGIN);
        String password = req.getParameter(PARAM_NAME_PASSWORD);

        if (LoginLogic.checkLogin(login, password)) {
            req.setAttribute("user", login);
            page = ConfigurationManager.getProperty("path.page.main");
        }else {
            req.setAttribute("errorLoginPassMessage",
                    MessageManager.getProperty("message.loginerror"));
            page = ConfigurationManager.getProperty("path.page.login");
        }
        return page;
    }
}
