package ru.yauheni.web.first.command;

import ru.yauheni.web.first.manager.ConfigurationManager;
import javax.servlet.http.HttpServletRequest;

public class EmptyCommand implements ActionCommand {

        public String execute(HttpServletRequest request) {
            return ConfigurationManager.getProperty("path.page.login");
        }
}
