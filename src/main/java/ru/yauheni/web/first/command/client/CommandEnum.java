package ru.yauheni.web.first.command.client;

import ru.yauheni.web.first.command.ActionCommand;
import ru.yauheni.web.first.command.LoginCommand;
import ru.yauheni.web.first.command.LogoutCommand;

public enum CommandEnum {
    LOGIN {
        {
            this.currentCommand = new LoginCommand();
        }
    },
    LOGOUT {
        {
            this.currentCommand = new LogoutCommand();
        }
    };
    ActionCommand currentCommand;

    public ActionCommand getCurrentCommand() {
        return currentCommand;
    }
}
