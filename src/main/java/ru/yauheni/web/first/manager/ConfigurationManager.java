package ru.yauheni.web.first.manager;

import java.util.Locale;
import java.util.ResourceBundle;

public class ConfigurationManager {

    private static final ResourceBundle res = ResourceBundle.getBundle("config", Locale.ENGLISH);

    private ConfigurationManager() {}


    public static String getProperty(String s) {
        return res.getString(s);
    }
}
