package ru.yauheni.web.first.manager;

import java.util.Locale;
import java.util.ResourceBundle;

public class MessageManager {

    private final static ResourceBundle res = ResourceBundle.getBundle("massages", Locale.ENGLISH);

    private MessageManager() {}

    public static String getProperty(String s) {
        return res.getString(s);
    }

}
